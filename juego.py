# Ejecucion:
# sudo python3 juego.py

# Este programa fue desarrollado como PROYECTO 1 en equipo para el curso de IGC.
# ALUMNOS:
#  * TREVIÑO OLVERA JOSÉ (1730169)
#  * AMAYA ESCOBEDO JOSÉ LEONARDO (1730123)
#  * LARA CRUZ KARLA EDITH (1730034)
#  * MARTÍNEZ HERNÁNDEZ ANA CRISTINA (1730370)
#  * SOTO TREJO ELIZABETH (1730445)

# DESCRIPCIÓN:
# El proyecto consiste en el desarrollo del videojuego Physics Balls implementando la librería OpenGL.

# USO:
# El programa no recibe parámetros de entrada se puede ejecutar de la siguiente forma:
# Es necesario ejecutarlo con permisos de super-administrador para reproducir los sonidos.
	
# 	sudo python3 juego.py

# INTERACCIÓN:
# 	* Apuntar con el mouse para dirigir el disparo de la bola.
# 	* Presionar <click izq> del mouse para disparar la bola.
#	* Presionar <s> para Activar/Desactivar el sonido del juego.
#	* Presionar <r> para reiniciar el juego.
#	* Presionar <p> para Activar/Desactivar pausar el juego.


from OpenGL.GLUT import *       # GL Utilities Toolkit
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
import sys
import math
import random

from clases import escribirFuente,sonidoColision,sonidoTiro,sonidoItem,sonidoGameOver,sonidoReinicio
from clases import Punto,Figura,Pelota

# Valores resolucion de la ventana
AnchuraVentana=820
AlturaVentana=820
TELCA_ESCAPE = b'\x1b' #'\033'


##########################################################
#	DEFINICION DE VARIABLES GLOBALES
##########################################################

# CONFIGURACION DEL ESCENARIO
listaFiguras = []
listaPelotas = []

mouseX = 0
mouseY = 0

mouseXDir = 0
mouseYDir = 0

# CONFIGURACION DEL COMPORTAMIENTO
habilitarTiro = True
lanzar=False

posicionX = 0
posicionY = 0

UMBRAL_COLISION = 50
UMBRAL_MAXHITS = 10
SCORE = 0

pSalidaPelota = Punto(AnchuraVentana/2,AlturaVentana-100,6, (0,0,0) ) #p3EsqIzq


# CONFIGURACION DE LA ANIMACION
periodoAnimacion = 30 #100
velocidad = 15.0 #5
anguloRotacion = 0.0
llegaLimite = False
nCiclos = 0

nFrames = 0

# VARIABLES DEL CONTADOR
MILISEGUDOS = 1000
contador_segundos = 0
contador_minutos = 0
iterador = 0

# CONFIGURACION DEL MOVIMIENTO
dirX = 0.0
dirY = 0.0
dirX_base = 0.0
dirY_base = 0.0

numFuera = 0
numColocadas = 0
posLanzamientoY = 20

SUBIR_TABLERO = False
contSubida = 0
PAUSA = False

ACTIVAR_SONIDO = True

# UTILIDAD:
#	Función principal del programa primera en orden de ejecicion.
# PARÁMETROS:
#	Sin parámetros.
def main():

	global AnchuraVentana
	global AlturaVentana

	# Mostrar informacion del programa
	init()

	# numFigs,nfilas
	generarEscenario(5,4)

	glutInit(sys.argv)
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)
	glutInitWindowPosition(100, 100)
	glutInitWindowSize(AnchuraVentana, AlturaVentana)
	glutCreateWindow(b"Juego - Physics Balls")

	# Funcion para capturar la entrada del teclado
	glutKeyboardFunc(keyPressed)

	# Seleccionar color blanco de fondo
	glClearColor(0.0,0.0,0.0, 0.0)

	# Configurar funcion de despliegue
	glutDisplayFunc(displayMe)
	glutPassiveMotionFunc(capturaMovMouse)
	glutMouseFunc(capturaMouse)
	glutReshapeFunc(resize)

	# Configurar la funcion de animacion
	glutTimerFunc(5, animar, 1)

	#configuracion del timer
	glutTimerFunc(MILISEGUDOS,timer,0)
	
	glutMainLoop()


# UTILIDAD:
#	Función configurada para el despliegue de los elementos del programa (manejada por OpenGL).
# PARÁMETROS:
#	Sin parámetros.
def displayMe():
	global PAUSA

	glClear(GL_COLOR_BUFFER_BIT)

	
	dibujarInterfaz()

	dibujarEscenario()

	comprobarColision()

	comprobarPerder()


	
	glFlush()

# UTILIDAD:
#	Función para dibujar todos los elementos(figuras y pelotas) dentro del escenario.
# PARÁMETROS:
#	Sin parámetros.
def dibujarEscenario():
	global listaFiguras,listaPelotas

	# Dibujar la lista de figuras
	for fila in listaFiguras:
		for fig in fila:
			fig.dibujar()

			# Comprobar si el objeto es el item + 1
			if	fig.getNumVertices() == 100:
				fig.dibujarCruz()

			# Dibujar etiqueta de hitpoints
			if (fig.getNumVertices() < 100):
				fig.dibujarEtiqueta()

	# Dibujar la lista de pelotas
	for pelota in listaPelotas:
		pelota.getFigura().dibujar()


# UTILIDAD:
#	Función para capturar el movimiento pasivo del mouse en la interfaz.
# PARÁMETROS:
#	<x> - Posición del mouse en el eje X (Parámetro controlado por OpenGL).
#	<y> - Posición del mouse en el eje Y (Parámetro controlado por OpenGL).
def capturaMovMouse(x,y):
	global mouseX,mouseY,AlturaVentana

	# Capturar y actualizar posicion del mouse pasiva
	mouseX,mouseY = x,AlturaVentana -y

	glutPostRedisplay()

# UTILIDAD:
#	Función para capturar el evento del mouse en la interfaz.
# PARÁMETROS:
#	<button> - Código de botón presionado (Parámetro controlado por OpenGL).
#	<state> - Código de estado del botón presionado (Parámetro controlado por OpenGL).
#	<x> - Posición del mouse en el eje X (Parámetro controlado por OpenGL).
#	<y> - Posición del mouse en el eje Y (Parámetro controlado por OpenGL).
def capturaMouse(button, state, x, y):
	global mouseX,mouseY,mouseXDir,mouseYDir,lanzar,habilitarTiro,listaPelotas,velocidad,dirX_base,dirY_base,PAUSA

	if not PAUSA:
		# Comprobar click del mouse
		if (button == GLUT_LEFT_BUTTON and state == GLUT_DOWN):
			# Si el tiro esta habilitado
			if habilitarTiro:
				#print('LANZAR')
				
				if ACTIVAR_SONIDO:
					sonidoTiro()

				# Arrancar lanzamiento de la pelota
				lanzar = True
				mouseXDir = mouseX
				mouseYDir = mouseY
				habilitarTiro = False

				# Movimiento continuo
				dirX,dirY = prolongarRecta(pSalidaPelota.getX(),pSalidaPelota.getY(),mouseXDir,mouseYDir,600)
				dirX_base,dirY_base = mouseXDir,mouseYDir

				# Fijar el mismo punto a todas las pelotas
				for pelota in listaPelotas:
					pelota.setVelocidad(velocidad)
					pelota.setDirX(dirX)
					pelota.setDirY(dirY)
	

	glutPostRedisplay()

# UTILIDAD:
#	Función para generar el primer escenario del juego.
# PARÁMETROS:
#	<numFigs> - Número máximo de figuras por fila.
#	<nfilas> - Número de filas a generar.
def generarEscenario(numFigs,nfilas):
	global listaFiguras,listaPelotas,UMBRAL_MAXHITS,velocidad

	listaFiguras = []
	tamano = 30
	figsVertices = [3,4,5,6,100]

	y = 60
	limY = nfilas * 2*tamano

	while y <= limY:
		numFigsRandom = random.randrange(1, 2) #random.randrange(1, 8)
		filaFiguras = []
		#fila = if len(listaFiguras) > 0: len(listaFiguras)-1 else 0

		for i in range(0,numFigsRandom):
			xRandom = random.randrange(40+tamano, AnchuraVentana-40-tamano)
			yRandom = y #random.randrange(-80+tamano, limY-tamano)
			hitsRandom = random.randrange(1, UMBRAL_MAXHITS)

			intentos = 0
			queda = False
			while (not queda) and intentos < 100:
				xRandom = random.randrange(40+tamano, AnchuraVentana-40-tamano) #random.randrange(40+tamano, 90-tamano)
				
				if librePosicionar(filaFiguras,xRandom):
					queda = True

				intentos+=1

			#print('Intentos: ',intentos)

			# Si despues de 100 intentos de posionar no se encuentra espacio no dibujar figura
			if intentos == 100:
				continue

			

			verticesRandom = figsVertices[random.randrange(0, len(figsVertices)-1)]
			colorRandom = (random.randrange(0, 255),random.randrange(0, 255),random.randrange(0, 255))

			#xVal, yVal,nVertices,numHits,radioVal,colorVal
			fig = Figura(xRandom,yRandom, verticesRandom,hitsRandom,tamano, colorRandom)

			# Agregar figura a la fila de figuras
			filaFiguras.append(fig)

		# Agregar ffila de figuras a al arreglo de figuras
		listaFiguras.append(filaFiguras)

		# Incrementar la coordenada y de la fila
		y += 2*tamano


	# INSERTAR LA PRIMERA PELOTA EN EL ESCENARIO
	pelota = Pelota(AnchuraVentana/2,AlturaVentana-80,1,10,velocidad)

	listaPelotas.append(pelota)
	#listaPelotas.append(Pelota(AnchuraVentana/2,AlturaVentana-80,1,10,velocidad))

# UTILIDAD:
#	Función para generar y agregar una nueva fila de figuras al tablero del juego.
# PARÁMETROS:
#	Sin parámetros.
def generarNuevaFila():
	global listaFiguras,UMBRAL_MAXHITS,SUBIR_TABLERO

	tamano = 30
	figsVertices = [3,4,5,6,100]

	#y = 60
	y=0

	numFigsRandom = random.randrange(1, 4) #random.randrange(1, 8) 
	filaFiguras = []

	randomPelotaPlus = random.randrange(1,4) #generar aletorio para hacer o no una item de pelota
	banPelotaPlus = False

	for i in range(0,numFigsRandom):

		# Generacion del item +1
		if randomPelotaPlus >= 3 and banPelotaPlus == False:	
			xRandom = random.randrange(40+tamano, AnchuraVentana-40-tamano)
			fig = Figura(xRandom,y,100,1,15,(255,255,255))
			filaFiguras.append(fig)
			banPelotaPlus = True

		xRandom = random.randrange(40+tamano, AnchuraVentana-40-tamano)
		yRandom = y
		hitsRandom = random.randrange(1, UMBRAL_MAXHITS)

		intentos = 0
		queda = False
		while (not queda) and intentos < 100:
			xRandom = random.randrange(40+tamano, AnchuraVentana-40-tamano)
			
			if librePosicionar(filaFiguras,xRandom):
				queda = True

			intentos+=1

		# Si despues de 100 intentos de posionar no se encuentra espacio no dibujar figura
		if intentos == 100:
			continue

		verticesRandom = figsVertices[random.randrange(0, len(figsVertices)-1)]
		colorRandom = (random.randrange(0, 255),random.randrange(0, 255),random.randrange(0, 255))

		#xVal, yVal,nVertices,numHits,radioVal,colorVal
		fig = Figura(xRandom,yRandom, verticesRandom,hitsRandom,tamano, colorRandom)

		# Agregar figura a la fila de figuras
		filaFiguras.append(fig)

	# Agregar fila de figuras a al arreglo de figuras
	listaFiguras.append(filaFiguras)

	# Incrementar la complejidad en cada subida
	UMBRAL_MAXHITS += 5

	# Activar bandera para subir figuras
	SUBIR_TABLERO = True


# UTILIDAD:
#	Función para comprobar si es posible dibujar una figura sin que exista solapamiento entre las demás.
# PARÁMETROS:
#	<fila> - Lista de figuras (lista de instancias de la clase figura).
#	<xRandom> - Posición aleatoria en el eje X para comprobar generación libre.
def librePosicionar(fila, xRandom):
	global listaFiguras

	if len(fila) > 0: 
		for fig in fila:
			# Si el espacio está ocupado e interfiere con alguna figura
			if xRandom >= fig.getX()-(2*fig.getRadio()) and xRandom <= fig.getX()+2*fig.getRadio():
				return False


	return True

# UTILIDAD:
#	Función configurada para crear la animación de los objetos del juego, controlada por Timer.
# PARÁMETROS:
#	<value> - Parámetro controlado por OpenGL. 
def animar(value):
	global lanzar,listaPelotas,mouseXDir,mouseYDir,AnchuraVentana,SUBIR_TABLERO,contSubida,habilitarTiro,numColocadas,velocidad,posLanzamientoY,PAUSA
	
	if not PAUSA:
		# Recorrer toda la lista de pelotas en el escenario
		for pelota in listaPelotas:

			# Si esta lanzada
			if lanzar:
				if pelota.getLlegaMouse() == False:
					# Mover pelota
					pelota.mover()
				else:
					#print('FINNNNN')
					#habilitarTiro = True
					lanzar = False
			
			# Mover pelota en movimiento libre
			if pelota.getMovimientoLibre():
				pelota.mover()

			#print('Vel: ',pelota.getFigura().getY())

			# Ejecutar animacion de retorno a la posicion de inicio
			if pelota.getAnimReiniciar():
				actualX = pelota.getFigura().getX()
				actualY = pelota.getFigura().getY()

				# Si ya llego al punto de reinicio desactivar la animacion
				if pelota.getLlegaMouse(): #actualX == pSalidaPelota.getX() and actualY == pSalidaPelota.getY():
					
					#print('Termina animacion')
					pelota.setAnimReiniciar(False)
					pelota.setLlegaMouse(False)
					# Reiniciar la posicion de salida de la pelota
					pelota.getFigura().setX(pSalidaPelota.getX())
					pelota.getFigura().setY(pSalidaPelota.getY()) #+ (posLanzamientoY*(len(listaPelotas)-1)))

					pelota.setLlegaMouse(False)

					numColocadas += 1

					# Si ya estan listas y colocadas todas las pelotas
					if numColocadas == len(listaPelotas):
						#print('----- TODAS LLEGAN  ----')
						#lanzar = False
						habilitarTiro = True
						numColocadas = 0
						pelota.setVelocidad(velocidad)
						# Generar una nueva fila en el escenario y subir las anteriores
						generarNuevaFila()


				elif pelota.getDirX() == pSalidaPelota.getX() and pelota.getDirY() == pSalidaPelota.getY():
					# Mover pelota hasta el punto de salida
					pelota.mover()
				else:
					if actualX < AnchuraVentana-20:
						pelota.getFigura().setX(actualX + 20.0)
					else:
						if actualY < AlturaVentana-40:
							pelota.getFigura().setY(actualY + 20.0)
						else:
							# Dirigir pelota hacia el punto de salida inicial
							pelota.setMovimientoLibre(False)
							pelota.setLlegaMouse(False)

							pelota.setDirX(pSalidaPelota.getX())
							pelota.setDirY(pSalidaPelota.getY())


		# Si se activa para subir el tablero
		if SUBIR_TABLERO:
			# Recorrer lista de figuras para aumentar y y subir tablero
			for fila in listaFiguras:
				for fig in fila:
					actualY = fig.getY()
					fig.setY(actualY + 1.5)

			contSubida += 1.5

			# Comprobar si termina la animacion
			if contSubida >= 60.0:
				SUBIR_TABLERO = False
				contSubida = 0



	glutPostRedisplay()
	glutTimerFunc(periodoAnimacion, animar, 1)

# UTILIDAD:
#	Función para calcular la posición de un punto alrededor de una circunferencia basado en la rotación
# PARÁMETROS:
#	<punto> :Punto - Instancia de la clase Punto, hace referencia al punto con el centro de la circunferencia
#	<radio> :float - Radio de la circunferencia a evaluar
def comprobarColision():
	global listaPelotas,listaFiguras,habilitarTiro,movimientoLibre,SCORE,dirX,dirY,AlturaVentana,AnchuraVentana,pSalidaPelota,velocidad,numFuera,numColocadas,dirX_base,dirY_base

	i=0
	j=0

	figurasDestruir = []
	# Recorrer todas las pelotas del escenario
	for pelota in listaPelotas:
		i=0
		# Recorrer todas las figuras del escenario
		for fila in listaFiguras:
			j=0
			for fig in fila:
				# Comprobar colision de pelota con figura
				if colisiona(pelota.getFigura().getX(),pelota.getFigura().getY(),fig.getX(),fig.getY()) and (not pelota.getAnimReiniciar()):
					#print('COLISION!!')

					colorActual = fig.getColor()
					hitsActual = fig.getNumHits()

					# Restar contador de hits
					fig.setNumHits(hitsActual-1)

					# Contar 1 al score
					SCORE = SCORE +1

					# Cambiar el color de la figura
					fig.setColor( (colorActual[0]-10,colorActual[1]-10,colorActual[2]-10 ))

					pelota.setMovimientoLibre(True)

					randMovX = random.randrange(0, AnchuraVentana)
					randMovY = random.randrange(int(pelota.getFigura().getY()), AlturaVentana)

					# Movimiento continuo
					dirX,dirY = prolongarRecta(pelota.getFigura().getX(),pelota.getFigura().getY(),randMovX,randMovY,600)
					dirX_base,dirY_base = randMovX,randMovY

					# Cambiar la direccion de la pelota de forma aleatoria
					pelota.setDirX(dirX)
					pelota.setDirY(dirY)

					# Si el contador de hits es igu
					if fig.getNumHits() <= 0:
						#figurasDestruir.append([i,j])
						figurasDestruir.append(fig)

					# Reproducir sonido en la colision
					if ACTIVAR_SONIDO:
						sonidoColision()

					# Es un item + 1
					if fig.getNumVertices() == 100:
						# Agregar una nueva pelota al escenario
						listaPelotas.append(Pelota(AnchuraVentana/2,720,1,10,0))
						numFuera += 1
						numColocadas += 1
						
						if ACTIVAR_SONIDO:
							sonidoItem()


					j = j + 1

			i = i + 1

		# Comprobar colision con los bordes del escenario
		pelotaX = pelota.getFigura().getX()
		pelotaY = pelota.getFigura().getY()
		pelotaRadio = pelota.getFigura().getRadio()

		limiteMaxX = AnchuraVentana-40-pelota.getFigura().getRadio()
		limiteMinX = 40

		limiteMaxY = pSalidaPelota.getY() #AlturaVentana-100
		limiteMinY = 0#20#0

		colisionBorde = False

		randMovX = random.randrange(0+220, AnchuraVentana-220) #randMovX = random.randrange(0, AnchuraVentana)
		try:
			randMovY = random.randrange(int(pelota.getFigura().getY()), AlturaVentana)
		except:
			randMovY = 0

		mDir = calcularPendiente(pelotaX,pelotaY,dirX_base,dirY_base)

		#print('Pendiente: ',mDir)

		if (pelotaX >= limiteMaxX) and (not pelota.getAnimReiniciar()):
			pelotaX = limiteMaxX #- (5*pelotaRadio)
			colisionBorde = True
			
			# Comprobar direccion del rebote en eje Y
			if mDir > 0:#if dirY > 0 and dirY < (AlturaVentana/2):
				randMovY = random.randrange(int(pelota.getFigura().getY()), AlturaVentana)
			else:
				try:
					randMovY = random.randrange(0, int(pelota.getFigura().getY()))
				except:
					randMovY = 0

		elif (pelotaX <= limiteMinX):
			pelotaX = limiteMinX #+ pelotaRadio
			colisionBorde = True

			# Comprobar direccion del rebote en eje Y
			#if dirY > 0:
			if mDir < 0: #if dirY > 0 and dirY < AlturaVentana/2:
				randMovY = random.randrange(int(pelota.getFigura().getY()), AlturaVentana)
			else:
				try:
					randMovY = random.randrange(0, int(pelota.getFigura().getY()))
				except:
					randMovY = 0

		#if (pelotaY > limiteMaxY and not (pelotaY <= 740)) and (not pelota.getAnimReiniciar()):
		if (pelotaY > limiteMaxY and not (pelotaY <= 740) and habilitarTiro==False and (not pelota.getAnimReiniciar()) ):
			#print('Entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
			pelotaY = limiteMaxY #- pelotaRadio
			colisionBorde = True

			#randMovX = random.randrange(0+120, AnchuraVentana-120)
			randMovY = random.randrange(0, int(pelota.getFigura().getY()))

		elif (pelotaY <= limiteMinY):
			#pelotaY = limiteMinY #+ pelotaRadio

			#print('Entra reiniciar')
			
			# Reiniciar posicion pelota
			reiniciarPosicionPelota(pelota)
			


		if colisionBorde:
			# Actualizar las posiciones en caso de colision con los bordes
			pelota.getFigura().setX(pelotaX)
			pelota.getFigura().setY(pelotaY)

			# Desactivar movimiento al mouse
			pelota.setLlegaMouse(True)
			pelota.setMovimientoLibre(True)

			# Movimiento continuo
			dirX,dirY = prolongarRecta(pelota.getFigura().getX(),pelota.getFigura().getY(),randMovX,randMovY,600)
			dirX_base,dirY_base = randMovX,randMovY

			# Cambiar direccion de la pelota
			pelota.setDirX(dirX)
			pelota.setDirY(dirY)
			
			if ACTIVAR_SONIDO:
				sonidoColision()

		i = i + 1


	# Destruir las figuras del ESCENARIO
	for d in figurasDestruir:
		for fila in listaFiguras:
			for fig in fila:
				if fig == d:
					# Eliminar figura
					fila.remove(d)

	#for d in figurasDestruir:
	#	listaFiguras[d[0]].pop(d[1])

def colisiona(x1,y1,x2,y2):
	global UMBRAL_COLISION
 
	# Comprobar colision con el rombo
	if ( abs(x1-x2) + abs(y1-y2) <= UMBRAL_COLISION):
		return True
	else:
		return False

def comprobarPerder():
	global listaFiguras,listaPelotas,habilitarTiro,lanzar,PAUSA
	pierde = False
	# Recorrer lista de figuras y comprobar si alguna toca el limite
	for fila in listaFiguras:
		for fig in fila:
			if fig.getY() >= AlturaVentana-100:
				pierde = True

	if pierde:
		escribirFuente(GLUT_BITMAP_HELVETICA_18,"¡HAS PERDIDO! PRESIONAR r para reiniciar",AnchuraVentana/2-200,790)
		
		if ACTIVAR_SONIDO and (not PAUSA):
			sonidoGameOver()

		for pelota in listaPelotas:
			pelota.setMovimientoLibre(False)
		
		# Deshabilitar tiro
		habilitarTiro = False
		lanzar = False
		PAUSA = True

def reiniciarPosicionPelota(pelota):
	global pSalidaPelota,lanzar,habilitarTiro,listaPelotas,numFuera
	# Contar una bola fuera
	numFuera += 1

	# Levantar pelota antes de la animacion
	pelota.getFigura().setY(20)
	# Reinciar direccion al mouse
	pelota.setLlegaMouse(False)
	# Desactivar movimiento libre
	pelota.setMovimientoLibre(False)
	# Activar animacion de reinicio
	pelota.setAnimReiniciar(True)
	
	
	# Si todas las pelotas estan fuera habilitar el tiro de nuevo
	if numFuera == len(listaPelotas):
		lanzar = False
		#habilitarTiro = True
		# Reiniciar contador de bolas fuera
		numFuera = 0

	if ACTIVAR_SONIDO:
		sonidoReinicio()

# UTILIDAD:
#	Función para dibujar la interfaz y el escenario del juego.
# PARÁMETROS:
#	Sin parámetros
def dibujarInterfaz():
	global mouseX,mouseY,habilitarTiro,AnchuraVentana,AlturaVentana,SCORE,listaPelotas,PAUSA

	########## DIBUJAR SEGMENTOS ##############################
	# Seleccionar color gris
	glColor3ub(86,86,86)
	p1SuperiorIzq = Punto(40,AlturaVentana-40, 6, (0,0,0))
	p2InferiorIzq = Punto(40,40,6, (0,0,0))

	p1SuperiorDer = Punto(AnchuraVentana-40,AlturaVentana-40,6, (0,0,0) )
	p2InferiorDer = Punto(AnchuraVentana-40,40,6, (0,0,0))

	p3EsqIzq = Punto(AnchuraVentana/2 -20,AlturaVentana-100,6, (0,0,0) )
	p3EsqDer = Punto(AnchuraVentana/2 +20,AlturaVentana-100,6, (0,0,0) )

	p1PunteadoIzq = Punto(p1SuperiorIzq.getX(),p3EsqIzq.getY(),6, (0,0,0) )
	p1PunteadoDer = Punto(p1SuperiorDer.getX(),p3EsqDer.getY(),6, (0,0,0) )

	pSalidaPelota = Punto(AnchuraVentana/2,p3EsqIzq.getY(),6, (0,0,0) )
	pMouse = Punto(mouseX,mouseY,6, (0,0,0) )
	
	dibujarSegmento(p1SuperiorIzq,p2InferiorIzq)
	dibujarSegmento(p1SuperiorDer,p2InferiorDer)

	dibujarSegmento(p1SuperiorIzq,p3EsqIzq)
	dibujarSegmento(p1SuperiorDer,p3EsqDer)

	glEnable(GL_LINE_STIPPLE)
	glLineStipple(1, 0xff)
	dibujarSegmento(p1PunteadoIzq,p3EsqIzq)
	dibujarSegmento(p1PunteadoDer,p3EsqDer)

	glDisable(GL_LINE_STIPPLE)
	
	#########################################################
	#	DIBUJAR TRAYECTORIA TIRO
	#########################################################

	if habilitarTiro and (not PAUSA):
		# Seleccionar color blanco
		glColor3ub(255,255,255)
		glEnable(GL_LINE_STIPPLE)
		glLineStipple(1, 0xff)
		dibujarSegmento(pSalidaPelota,pMouse)
		glDisable(GL_LINE_STIPPLE)

	# MOSTRAR SCORE
	# Seleccionar color blanco
	glColor3ub(255,255,255)
	escribirFuente(GLUT_BITMAP_HELVETICA_18,"SCORE:",AnchuraVentana-170,AlturaVentana-85)
	escribirFuente(GLUT_BITMAP_HELVETICA_18,str(SCORE),AnchuraVentana-85,AlturaVentana-85)

	escribirFuente(GLUT_BITMAP_HELVETICA_18,"PELOTAS: "+str(len(listaPelotas)),AnchuraVentana-160,AlturaVentana-30)

	pausaMensaje = ""
	if PAUSA:
		pausaMensaje = "EN PAUSA"

	escribirFuente(GLUT_BITMAP_HELVETICA_18,pausaMensaje,100,AlturaVentana-30)

	dibujarTimer()

# UTILIDAD:
#	Función para reiniciar la posicion del escenario y la animación del programa.
# PARÁMETROS:
#	Sin parámetros
def reiniciar():
	global contador_segundos,contador_minutos,iterador,UMBRAL_MAXHITS,listaFiguras,listaPelotas,SCORE,habilitarTiro,lanzar,PAUSA
	
	contador_segundos = 0
	contador_minutos = 0
	iterador = 0

	UMBRAL_MAXHITS = 50

	listaFiguras = []
	listaPelotas = []

	SCORE = 0
	PAUSA = False

	habilitarTiro = True
	lanzar=False
	
	generarEscenario(5,4)




# UTILIDAD:
#	Funcion para trazar un segmento de línea entre los puntos <p1> y <p2>. 
# PARÁMETROS:
#	<p1> :Punto - Punto 1, instancia de la clase punto. 
#	<p2> :Punto - Punto 2, instancia de la clase punto.
def dibujarSegmento(p1,p2):
	# Hacer mas ancha la linea
	glLineWidth(3.0)
	# Dibujar linea segmento entre puntos
	glBegin(GL_LINES)
	glVertex3f(p1.getX(), p1.getY(), 0.0)
	glVertex3f(p2.getX(), p2.getY(), 0.0)
	glEnd()
	# Reiniciar ancho de la linea
	glLineWidth(1.0)

# UTILIDAD:
#	Función para crear el contador de tiempo en la interfaz.
# PARÁMETROS:
#	<value> - Parámetro controlado por OpenGL.
def timer(value):
	global iterador, contador_segundos, contador_minutos ,MILISEGUDOS,PAUSA
	
	if not PAUSA:
		if iterador == 0:
			contador_segundos = contador_segundos + 1
		elif iterador == 1:
			contador_segundos = contador_segundos +0
		else:
			contador_segundos = 0

		if contador_segundos > 59:
			contador_minutos = contador_minutos + 1
			contador_segundos = 0

	glutPostRedisplay()
	glutTimerFunc(MILISEGUDOS,timer,contador_segundos)

# UTILIDAD:
#	Función para dibujar etiquetas del contador de tiempo en la interfaz.
# PARÁMETROS:
#	Sin parámetros
def dibujarTimer():
	global contador_segundos,contador_minutos

	str_segundos = str(contador_segundos)
	str_minutos = str(contador_minutos)

	if contador_segundos <10:
		str_segundos = "0"+str_segundos

	if contador_minutos <10:
		str_minutos = "0"+str_minutos
	

	glColor3f(1.0,1.0,1.0)
	#glRasterPos3f(60.0, 740.0, 0.0)
	#writeBitmapString(GLUT_BITMAP_8_BY_13, "Timer")
	escribirFuente(GLUT_BITMAP_HELVETICA_18,"Timer",60,745)
	#glRasterPos3f(100.0, 725.0, 0.0)
	#writeBitmapString(GLUT_BITMAP_8_BY_13, str(contador_segundos))
	escribirFuente(GLUT_BITMAP_HELVETICA_18, str_segundos,95,725)
	#glRasterPos3f(80.0, 725.0, 0.0)
	#writeBitmapString(GLUT_BITMAP_8_BY_13,":")
	escribirFuente(GLUT_BITMAP_HELVETICA_18,":",85,725)

	#glRasterPos3f(60.0, 725.0, 0.0)
	#writeBitmapString(GLUT_BITMAP_8_BY_13, str(contador_minutos))
	escribirFuente(GLUT_BITMAP_HELVETICA_18,str_minutos,60,725)

# UTILIDAD:
#	Función para obtener un punto de recta prolongado en una distancia.
# PARÁMETROS:
#	<x1> - Coordenada X del primer punto origen.
#	<y1> - Coordenada Y del primer punto origen.
#	<x2> - Coordenada X del segundo punto destino.
#	<y2> - Coordenada Y del segundo punto destino.
#	<distancia> - Distancia de prolongación de la recta.
def prolongarRecta(x1,y1,x2,y2,distancia):
	global AnchuraVentana
	# Salida pelota
	m = calcularPendiente(x1,y1,x2,y2)
	x = x2

	if x2 < (AnchuraVentana/2):
		x = x - distancia
	else:
		x = x + distancia

	y = m*x -(m*x1) + y1

	return x,y

# UTILIDAD:
#	Función para capturar la entrada del teclado.
# PARÁMETROS:
#	<*args> :list - Lista con la tecla presionada (manejado por OpenGL)
def keyPressed(*args):
	global ACTIVAR_SONIDO,PAUSA

	# Reinciar el escenario al presionar tecla r
	if args[0] == b'r':
		reiniciar()

	# Activar/Desactivar sonido
	if args[0] == b's':
		ACTIVAR_SONIDO = not ACTIVAR_SONIDO

	# Activar/Desactivar pausa
	if args[0] == b'p':
		PAUSA = not PAUSA

	# Terminar la ejecucion del programa si presiona la tecla ESC
	if args[0] == TELCA_ESCAPE:
		sys.exit()


# UTILIDAD:
#	Función para configurar comportamiento al estirar ventana.
# PARÁMETROS:
#	<w> :float - Ancho de la ventana (manejado por OpenGL).
#	<h> :float - Alto de la ventana (manejado por OpenGL).
def resize(w, h):
	glViewport(0, 0, w, h)
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	#glOrtho(-100.0, 100.0, -100.0, 100.0, -1.0, 1.0)

	glOrtho(0.0,AnchuraVentana,0.0,AlturaVentana,-500.0,500.0);
	glMatrixMode(GL_MODELVIEW)
	glLoadIdentity()


# UTILIDAD:
#	Función para obtener la pendiente de la recta entre dos puntos en el plano.
# PARÁMETROS:
#	<x1> - Coordenada X del primer punto origen.
#	<y1> - Coordenada Y del primer punto origen.
#	<x2> - Coordenada X del segundo punto destino.
#	<y2> - Coordenada Y del segundo punto destino.
def calcularPendiente(x1,y1,x2,y2):
	m = 0.0
	try:
		m = (y2-y1)/(x2-x1)
	except:
		m = 0.0

	return m

# UTILIDAD:
#	Función para desplegar la información general del programa en pantalla.
# PARÁMETROS:
#	Sin parámetros
def init():
	print("""
Este programa fue desarrollado como PROYECTO 1 en equipo para el curso de IGC.
ALUMNOS:
 * TREVIÑO OLVERA JOSÉ (1730169)
 * AMAYA ESCOBEDO JOSÉ LEONARDO (1730123)
 * LARA CRUZ KARLA EDITH (1730034)
 * MARTÍNEZ HERNÁNDEZ ANA CRISTINA (1730370)
 * SOTO TREJO ELIZABETH (1730445)

DESCRIPCIÓN:
El proyecto consiste en el desarrollo del videojuego Physics Balls implementando la librería OpenGL.

USO:
El programa no recibe parámetros de entrada se puede ejecutar de la siguiente forma:
Es necesario ejecutarlo con permisos de super-administrador para reproducir los sonidos.
	
	sudo python3 juego.py

INTERACCIÓN:
	* Apuntar con el mouse para dirigir el disparo de la bola.
	* Presionar <click izq> del mouse para disparar la bola.
	* Presionar <s> para Activar/Desactivar el sonido del juego.
	* Presionar <r> para reiniciar el juego.
	* Presionar <p> para Activar/Desactivar pausar el juego.

""")

main()
