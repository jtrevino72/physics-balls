from OpenGL.GLUT import *       # GL Utilities Toolkit
from OpenGL.GL import *
from OpenGL.GLU import *
import math
import os
import subprocess
from subprocess import DEVNULL 

#from subprocess import DEVNULL, STDOUT, check_call

# Valores resolucion de la ventana
AnchuraVentana=820
AlturaVentana=820

# UTILIDAD:
#	Función para dibujar un texto en la pantalla.
# PARÁMETROS:
#	<fuente> :OPENGL_FONT - Fuente seleccionada para escribir (manejada por OpenGL).
#	<texto> :string - Texto plano a escribir.
#	<x> :float - Coordenada x para escribir texto.
#	<y> :float - Coordenada y para escribir texto.
def escribirFuente(fuente,texto,x,y):
	# Fuente en color negro
	#glColor3f(0.0,0.0,0.0)
	# Fijar posicion de escritura
	glRasterPos3f(x,y,0.0)

	# Escribir caracter por caracter
	for ch in texto:
		glutBitmapCharacter(fuente,ctypes.c_int(ord(ch)))

# Definicion de la clase Figura
class Pelota:
	# Atributos de la figura
	#posicion = Punto(0,0)
	figura = None
	llegaMouse = False
	movimientoLibre = False
	velocidad  = 0

	dirX = 0.0
	dirY = 0.0

	animReiniciar = False

	# Constructor con parametros
	def __init__(self, xVal,yVal,nHits,radioVal,vel):
		# Crear figura del circulo de la pelota
		self.figura = Figura(xVal,yVal,100, nHits ,radioVal, (255,255,255))
		self.velocidad = vel
		self.llegaMouse = False
		self.movimientoLibre = False
		self.dirX = 0.0
		self.dirY = 0.0

		self.animReiniciar = False
	
	# Metodo para desplegar el punto en la pantalla
	def dibujar(self):
		# Dibujar
		self.figura.dibujar()

	def mover(self):
		# Obtener posicion actual
		posX = self.figura.getX()
		posY = self.figura.getY()

		#Se calcula la diferencia entre la posicion actual en X y la posicion del destino en X 
		disX=self.dirX-posX

		#Se calcula la diferencia entre la posicion actual en Y y la posicion del destino en Y
		disY=self.dirY-posY 

		#Se obtiene el angulo entre la posicion actual de la bola y la posicion del destino
		angulo = math.atan2(disY,disX)

		#Se asigna la nueva posicion en x
		posX = posX + (self.velocidad*math.cos(angulo))
		
		#Se asigna la nueva posicion en y
		posY = posY + (self.velocidad*math.sin(angulo))

		# Actualizar posiciones
		self.figura.setX(posX)
		self.figura.setY(posY)

		# Revisar si llego al mouse
		if ((disX < 10 and disX > -10 ) and (disY < 10 and disY > -10)):
			self.llegaMouse=True
			print('LLEGA')
			#self.movimientoLibre = True
		else:
			self.llegaMouse=False

	def getFigura(self):
		return self.figura

	def getLlegaMouse(self):
		return self.llegaMouse

	def getMovimientoLibre(self):
		return self.movimientoLibre

	def getVelocidad(self):
		return self.velocidad

	def getDirX(self):
		return self.dirX

	def getDirY(self):
		return self.dirY

	def getAnimReiniciar(self):
		return self.animReiniciar

	def setLlegaMouse(self,llega):
		self.llegaMouse = llega

	def setMovimientoLibre(self,mov):
		self.movimientoLibre = mov

	def setVelocidad(self,vel):
		self.velocidad = vel

	def setDirX(self,dis):
		self.dirX = dis

	def setDirY(self,dis):
		self.dirY = dis

	def setAnimReiniciar(self, anim):
		self.animReiniciar = anim


# Definicion de la clase Punto
class Punto:
	# Atributos del punto
	x = 0
	y = 0
	size = 0
	color=(0,0,0)

	# Constructor con parametros
	def __init__(self, xVal, yVal,sizeVal,colorVal):
		self.x = xVal
		self.y = yVal
		self.size = sizeVal
		self.color = colorVal

	# Constructor con parametros
	def simple(self, xVal, yVal):
		return Punto(xVal, yVal,0,(0,0,0))
	
	# Metodo para desplegar el punto en la pantalla
	def dibujar(self):
		# Seleccionar el color
		glColor3ub(self.color[0],self.color[1],self.color[2])
		# Fijar tamano de punto
		glPointSize(self.size)
		glBegin(GL_POINTS)
		glVertex3f(self.x, self.y, 0.0)
		glEnd()

	def getX(self):
		return self.x
	
	def getY(self):
		return self.y

	def setX(self,xVal):
		self.x = xVal

	def setY(self,yVal):
		self.y = yVal


# Definicion de la clase Figura
class Figura:
	# Atributos de la figura
	x = 0
	y = 0
	numVertices = 0
	numHits = 0
	radio = 0
	color=(0,0,0)
	vertices = []

	# Constructor con parametros
	def __init__(self, xVal, yVal,nVertices,nHits,radioVal,colorVal):
		self.x = xVal
		self.y = yVal
		self.numVertices = nVertices
		self.numHits = nHits
		self.radio = radioVal
		self.color = colorVal
	
	# Metodo para desplegar el punto en la pantalla
	def dibujar(self):
		self.vertices = []

		# Seleccionar el color
		glColor3ub(self.color[0],self.color[1],self.color[2])

		#glBegin(GL_LINE_LOOP)
		glBegin(GL_TRIANGLE_FAN)
		t=0.0

		for i in range(0, self.numVertices):
			#glColor3ub(random.randint(0,255), random.randint(0,255), random.randint(0,255))
			x1= self.x + self.radio * math.cos(t)
			y1= self.y + self.radio * math.sin(t)
			glVertex3f(x1, y1, 0.0)

			self.vertices.append([x1,y1])

			t += 2 * math.pi / self.numVertices

		glEnd()

	#Metodo para dibujar Cruz del Item
	def dibujarCruz(self):
		glColor3ub(0,0,0)
		glBegin(GL_LINES)
		glVertex3f(self.x,self.y,0)
		glVertex3f(self.x+5,self.y,0)

		glVertex3f(self.x,self.y,0)
		glVertex3f(self.x-5,self.y,0)
		
		glVertex3f(self.x,self.y,0)
		glVertex3f(self.x,self.y+5,0)
		
		glVertex3f(self.x,self.y,0)
		glVertex3f(self.x,self.y-5,0)
		glEnd()

	def dibujarEtiqueta(self):
		glColor3ub(0,0,0)
		# Dibujar etiqueta de hits en la figura
		escribirFuente(GLUT_BITMAP_HELVETICA_18, str(self.numHits), self.x-8, self.y-1)

	def getVertices(self):
		return self.vertices

	def getRadio(self):
		return self.radio

	def getNumHits(self):
		return self.numHits

	def getNumVertices(self):
		return self.numVertices

	def getColor(self):
		return self.color

	def getNumHits(self):
		return self.numHits

	def getX(self):
		return self.x
	
	def getY(self):
		return self.y

	def setX(self,xVal):
		self.x = xVal

	def setY(self,yVal):
		self.y = yVal

	def setColor(self,color):
		self.color = color

	def setNumHits(self,hits):
		if hits < 0:
			hits = 0
		
		self.numHits = hits


def sonidoColision():
	with open(os.devnull, 'w') as fp:
		va = subprocess.Popen(['aplay', './sonidos/Hit.wav'], stdout=fp)

def sonidoTiro():
	#check_call(['aplay', './sonidos/Shot.wav'], stdout=DEVNULL, stderr=STDOUT)
	#with open(os.devnull, 'w') as fp:
	#	va = subprocess.Popen(['aplay', './sonidos/Shot.wav'], stdout=fp)
	va = subprocess.Popen(['aplay', './sonidos/Shot.wav'], shell=False)

def sonidoItem():
	with open(os.devnull, 'w') as fp:
		va = subprocess.Popen(['aplay', './sonidos/Item.wav'], stdout=fp)

def sonidoGameOver():
	with open(os.devnull, 'w') as fp:
		va = subprocess.Popen(['aplay','./sonidos/Game_Over.wav'], stdout=fp)

def sonidoReinicio():
	with open(os.devnull, 'w') as fp:
		va = subprocess.Popen(['aplay', './sonidos/Return.wav'], stdout=fp)